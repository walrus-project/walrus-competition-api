package mailConfig

import (
	"crypto/tls"
	"fmt"
	"net/smtp"

	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"

	configErrors "gitlab.com/walrus-project/walrus-competition-api/internal/common/errors/config"
)

type Config struct {
	Host     string `mapstructure:"host" validate:"required,hostname"`
	Port     int16  `mapstructure:"port"`
	UseAuth  bool   `mapstructure:"useAuth"`
	User     string `mapstructure:"user" validate:"required_if=UseAuth true"`
	Password string `mapstructure:"password" validate:"required_if=UseAuth true"`
}

func (c *Config) Auth() smtp.Auth {
	if c.UseAuth {
		return smtp.PlainAuth("", c.User, c.Password, c.Host)
	}

	return nil
}

func (c *Config) Address() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

func (c *Config) TLSConfig() *tls.Config {
	return &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         c.Host,
	}
}

func New(validate *validator.Validate) (*Config, error) {
	mailViper := viper.New()
	mailViper.AddConfigPath("configs/mail")
	mailViper.SetConfigType("yaml")

	mailViper.SetDefault("port", 587)
	mailViper.SetDefault("useAuth", true)

	if err := mailViper.ReadInConfig(); err != nil {
		return nil, &configErrors.ReadConfigError{ConfigName: "mail", Err: err}
	}

	var mailConfig Config
	if err := mailViper.Unmarshal(&mailConfig); err != nil {
		return nil, &configErrors.UnmarshalError{ConfigName: "mail", Err: err}
	}

	if err := validate.Struct(mailConfig); err != nil {
		return nil, &configErrors.ValidationError{ConfigName: "mail", Err: err}
	}

	return &mailConfig, nil
}
