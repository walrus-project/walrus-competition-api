package ordersConfig

import (
	"net/mail"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"

	configErrors "gitlab.com/walrus-project/walrus-competition-api/internal/common/errors/config"
)

type NotifyAddressConfig struct {
	Name    string `mapstructure:"name" validate:"required"`
	Address string `mapstructure:"address" validate:"email"`
}

type Config struct {
	NotifyFrom       NotifyAddressConfig   `mapstructure:"notifyFrom" validate:"required"`
	NotifyTo         []NotifyAddressConfig `mapstructure:"notifyTo" validate:"dive"`
	NotifySubject    string                `mapstructure:"notifySubject" validate:"required"`
	TemplateFilePath string                `mapstructure:"templateFilePath" validate:"required"`
}

func (c *Config) NotifyFromAddress() string {
	from := mail.Address{Name: c.NotifyFrom.Name, Address: c.NotifyFrom.Address}

	return from.String()
}

func (c *Config) NotifyToAddresses() string {
	addresses := make([]string, 0, len(c.NotifyTo))

	for _, addr := range c.NotifyTo {
		to := mail.Address{Name: addr.Name, Address: addr.Address}

		addresses = append(addresses, to.String())
	}

	return strings.Join(addresses, ", ")
}

func New(validate *validator.Validate) (*Config, error) {
	ordersViper := viper.New()
	ordersViper.AddConfigPath("configs/orders")
	ordersViper.SetConfigType("yaml")

	if err := ordersViper.ReadInConfig(); err != nil {
		return nil, &configErrors.ReadConfigError{ConfigName: "orders", Err: err}
	}

	var ordersConfig Config
	if err := ordersViper.Unmarshal(&ordersConfig); err != nil {
		return nil, &configErrors.UnmarshalError{ConfigName: "orders", Err: err}
	}

	if err := validate.Struct(ordersConfig); err != nil {
		return nil, &configErrors.ValidationError{ConfigName: "orders", Err: err}
	}

	return &ordersConfig, nil
}
