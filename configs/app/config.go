package appConfig

import (
	"fmt"

	"github.com/spf13/viper"

	configErrors "gitlab.com/walrus-project/walrus-competition-api/internal/common/errors/config"
)

type Config struct {
	Host string `mapstructure:"host"`
	Port int16  `mapstructure:"port"`
}

func (c *Config) Address() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

func New() (*Config, error) {
	appViper := viper.New()
	appViper.AddConfigPath("configs/app")
	appViper.SetConfigType("yaml")

	appViper.SetDefault("host", "")
	appViper.SetDefault("port", 5000)

	if err := appViper.ReadInConfig(); err != nil {
		return nil, &configErrors.ReadConfigError{ConfigName: "app", Err: err}
	}

	var appConfig Config
	if err := appViper.Unmarshal(&appConfig); err != nil {
		return nil, &configErrors.UnmarshalError{ConfigName: "app", Err: err}
	}

	return &appConfig, nil
}
