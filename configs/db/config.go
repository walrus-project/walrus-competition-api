package databaseConfig

import (
	"fmt"

	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"

	configErrors "gitlab.com/walrus-project/walrus-competition-api/internal/common/errors/config"
)

type Config struct {
	Host     string `mapstructure:"host"`
	Port     int16  `mapstructure:"port"`
	User     string `mapstructure:"user" validate:"required"`
	Password string `mapstructure:"password" validate:"required"`
	Database string `mapstructure:"dbName" validate:"required"`
}

func (c *Config) DSN() string {
	return fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable", c.Host, c.User, c.Password, c.Database, c.Port)
}

func (c *Config) URL() string {
	return fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?sslmode=disable", c.User, c.Password, c.Host, c.Port, c.Database)
}

func New(validate *validator.Validate) (*Config, error) {
	dbViper := viper.New()

	dbViper.AddConfigPath("configs/db")
	dbViper.SetConfigType("yaml")

	dbViper.SetDefault("host", "127.0.0.1")
	dbViper.SetDefault("port", 5432)

	if err := dbViper.ReadInConfig(); err != nil {
		return nil, &configErrors.ReadConfigError{ConfigName: "db", Err: err}
	}

	var dbConfig Config
	if err := dbViper.Unmarshal(&dbConfig); err != nil {
		return nil, &configErrors.UnmarshalError{ConfigName: "db", Err: err}
	}

	if err := validate.Struct(dbConfig); err != nil {
		return nil, &configErrors.ValidationError{ConfigName: "db", Err: err}
	}

	return &dbConfig, nil
}
