package transformHelpers

import "gitlab.com/walrus-project/walrus-competition-api/internal/models"

func ToGender(gender string) models.Gender {
	var gd models.Gender

	switch gender {
	case "male":
		gd = "male"
	case "female":
		gd = "female"
	case "all":
		gd = "all"
	}

	return gd
}
