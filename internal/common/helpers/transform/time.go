package transformHelpers

import (
	"fmt"
	"time"
)

func ToDate(value string) (*time.Time, error) {
	t, err := time.Parse("2006-01-02", value)
	if err != nil {
		return nil, fmt.Errorf("failed to transform date value: %w", err)
	}

	return &t, nil
}
