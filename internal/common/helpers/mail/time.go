package mailHelpers

import "time"

func FormatDate(t time.Time) string {
	return t.Format("02-01-2006")
}

func YearsFromTime(t time.Time) int {
	now := time.Now()

	return now.Year() - t.Year()
}
