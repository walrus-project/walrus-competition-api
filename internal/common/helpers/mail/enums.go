package mailHelpers

import (
	"fmt"

	"github.com/volatiletech/null/v8"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

func GenderText(gender models.Gender) string {
	switch gender {
	case models.GenderAll:
		return "Любой"
	case models.GenderMale:
		return "Мужчина"
	case models.GenderFemale:
		return "Женщина"
	}

	return ""
}

func SwimmingStyleText(swimmingStyle models.SwimmingStyle) string {
	switch swimmingStyle {
	case models.SwimmingStyleBreaststroke:
		return "Брасс"
	case models.SwimmingStyleButterfly:
		return "Баттерфляй"
	case models.SwimmingStyleCrawl:
		return "Кроль"
	case models.SwimmingStyleFreestyle:
		return "Вольный"
	}

	return ""
}

func AgeGroupText(minAge null.Int, maxAge null.Int) string {
	if minAge.Valid && maxAge.Valid {
		return fmt.Sprintf("%d - %d", minAge.Int, maxAge.Int)
	} else if !minAge.Valid && maxAge.Valid {
		return fmt.Sprintf("до %d", maxAge.Int+1)
	} else if minAge.Valid && !maxAge.Valid {
		return fmt.Sprintf("от %d", minAge.Int)
	}

	return "Любой возраст"
}
