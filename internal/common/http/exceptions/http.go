package httpExceptions

import (
	"encoding/json"
	"net/http"
)

type Exception[TData any, TResponse any] interface {
	Error(w http.ResponseWriter, r TResponse)
	GetData() TData
	GetResponse() TResponse
}

type HttpException[TData any, TResponse any] struct {
	StatusCode int
	Code       string
	Message    string
	Data       TData
}

type HttpExceptionResponse struct {
	StatusCode int    `json:"status_code"`
	Code       string `json:"code"`
	Message    string `json:"message"`
}

func (e *HttpException[TData, TResponse]) Error(w http.ResponseWriter, r TResponse) {
	w.WriteHeader(e.StatusCode)
	json.NewEncoder(w).Encode(r)
}

func (e *HttpException[TData, TResponse]) GetData() TData {
	return e.Data
}

func (e *HttpException[TData, TResponse]) GetResponse() HttpExceptionResponse {
	return HttpExceptionResponse{
		StatusCode: e.StatusCode,
		Code:       e.Code,
		Message:    e.Message,
	}
}

func CreateHttpException[TData any, TResponse any](statusCode int, code string, message string, data TData) *HttpException[TData, TResponse] {
	return &HttpException[TData, TResponse]{
		StatusCode: statusCode,
		Code:       code,
		Message:    message,
		Data:       data,
	}
}
