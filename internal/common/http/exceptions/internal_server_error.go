package httpExceptions

import "net/http"

type InternalServerErrorException[TData any, TResponse any] struct {
	HttpException[TData, TResponse]
}

func CreateInternalServerErrorException[TData any, TResponse any](code string, message string, data TData) *InternalServerErrorException[TData, TResponse] {
	return &InternalServerErrorException[TData, TResponse]{
		HttpException[TData, TResponse]{
			StatusCode: http.StatusInternalServerError,
			Code:       code,
			Message:    message,
			Data:       data,
		},
	}
}
