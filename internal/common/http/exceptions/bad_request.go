package httpExceptions

import "net/http"

type BadRequestException[TData any, TResponse any] struct {
	HttpException[TData, TResponse]
}

func CreateBadRequestException[TData any, TResponse any](code string, message string, data TData) *BadRequestException[TData, TResponse] {
	return &BadRequestException[TData, TResponse]{
		HttpException[TData, TResponse]{
			StatusCode: http.StatusBadRequest,
			Code:       code,
			Message:    message,
			Data:       data,
		},
	}
}
