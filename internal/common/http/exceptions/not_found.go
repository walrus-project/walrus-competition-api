package httpExceptions

import "net/http"

type NotFoundException[TData any, TResponse any] struct {
	HttpException[TData, TResponse]
}

func CreateNotFoundException[TData any, TResponse any](code string, message string, data TData) *BadRequestException[TData, TResponse] {
	return &BadRequestException[TData, TResponse]{
		HttpException[TData, TResponse]{
			StatusCode: http.StatusNotFound,
			Code:       code,
			Message:    message,
			Data:       data,
		},
	}
}
