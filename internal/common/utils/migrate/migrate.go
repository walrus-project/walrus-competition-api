package migrateUtils

import (
	"database/sql"
	"fmt"
	"os"

	migrate "github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func Migrate(db *sql.DB) error {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return fmt.Errorf("failed to create driver instance for migrations: %w", err)
	}

	migrator, err := migrate.NewWithDatabaseInstance("file://migrations/", "postgres", driver)

	if err != nil && !os.IsNotExist(err) {
		return fmt.Errorf("failed to create migrator: %w", err)
	}

	if migrator != nil {
		err := migrator.Up()
		if err != migrate.ErrNoChange {
			return err
		}
	}

	return nil
}
