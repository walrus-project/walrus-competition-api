package exceptions

import (
	"net/http"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
)

const decodeRequestBodyErrorCode = "decode_request_body_error"

func CreateDecodeRequestBodyException(w http.ResponseWriter, err error) {
	decodeRequestBodyException := httpExceptions.CreateBadRequestException[any, httpExceptions.HttpExceptionResponse](decodeRequestBodyErrorCode, err.Error(), err)

	decodeRequestBodyException.Error(w, decodeRequestBodyException.GetResponse())
}
