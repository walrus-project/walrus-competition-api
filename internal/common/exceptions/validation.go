package exceptions

import (
	"net/http"

	"github.com/go-playground/validator/v10"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
)

type ValidationFieldError struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

type ValidationExceptionResponse struct {
	httpExceptions.HttpExceptionResponse
	Errors []ValidationFieldError `json:"errors"`
}

type ValidationException struct {
	httpExceptions.BadRequestException[[]validator.FieldError, ValidationExceptionResponse]
}

const requestBodyValidationErrorCode = "request_body_validation_error"

func (e *ValidationException) GetResponse() ValidationExceptionResponse {
	errors := make([]ValidationFieldError, 0, len(e.Data))

	for _, fieldError := range e.Data {
		errors = append(errors, ValidationFieldError{
			Field: fieldError.Field(),
			Error: fieldError.Error(),
		})
	}

	return ValidationExceptionResponse{
		HttpExceptionResponse: httpExceptions.HttpExceptionResponse{
			StatusCode: e.StatusCode,
			Code:       e.Code,
			Message:    e.Message,
		},
		Errors: errors,
	}
}

func CreateValidationException(w http.ResponseWriter, code string, message string, errors []validator.FieldError) {
	validationException := &ValidationException{
		BadRequestException: *httpExceptions.CreateBadRequestException[[]validator.FieldError, ValidationExceptionResponse](code, message, errors),
	}

	validationException.Error(w, validationException.GetResponse())
}

func CreateRequestBodyValidationException(w http.ResponseWriter, err error) {
	if errors, ok := err.(validator.ValidationErrors); ok {
		validationException := &ValidationException{
			BadRequestException: *httpExceptions.CreateBadRequestException[[]validator.FieldError, ValidationExceptionResponse](
				requestBodyValidationErrorCode,
				"Request body validation error",
				errors,
			),
		}

		validationException.Error(w, validationException.GetResponse())

		return
	}

	validationUnknownException := httpExceptions.CreateBadRequestException[any, httpExceptions.HttpExceptionResponse](requestBodyValidationErrorCode, err.Error(), err)

	validationUnknownException.Error(w, validationUnknownException.GetResponse())
}
