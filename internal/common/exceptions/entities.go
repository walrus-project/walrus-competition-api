package exceptions

import (
	"net/http"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
)

const (
	objectNotFoundErrorCode = "object_not_found"
)

func ObjectNotFoundException(w http.ResponseWriter) {
	notFoundException := httpExceptions.CreateNotFoundException[any, httpExceptions.HttpExceptionResponse](
		objectNotFoundErrorCode,
		"Object not found",
		nil,
	)

	notFoundException.Error(w, notFoundException.GetResponse())
}
