package exceptions

import (
	"fmt"
	"net/http"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
)

const (
	invalidIDParamInURLErrorCode = "invalid_id_param_in_url"
)

func InvalidIDParamInURLException(w http.ResponseWriter, err error) {
	invalidException :=
		httpExceptions.CreateBadRequestException[any, httpExceptions.HttpExceptionResponse](
			invalidIDParamInURLErrorCode,
			fmt.Sprintf("Invalid ID param in url: %s", err.Error()),
			err,
		)

	invalidException.Error(w, invalidException.GetResponse())
}
