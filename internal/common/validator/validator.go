package validator

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/go-playground/validator/v10"
)

func CreateValidator() (*validator.Validate, error) {
	validate := validator.New()

	if err := validate.RegisterValidation("gender", ValidateGender); err != nil {
		return nil, fmt.Errorf("failed to register gender validator: %w", err)
	}

	if err := validate.RegisterValidation("date", ValidateDate); err != nil {
		return nil, fmt.Errorf("failed to register date validator: %w", err)
	}

	validate.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]

		if name == "-" {
			return ""
		}

		return name
	})

	return validate, nil
}
