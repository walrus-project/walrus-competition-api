package validator

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

func ValidateDate(field validator.FieldLevel) bool {
	re := regexp.MustCompile("((19|20)\\d\\d)-(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[012])")
	value := field.Field().String()

	return re.MatchString(value)
}
