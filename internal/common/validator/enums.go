package validator

import (
	"github.com/go-playground/validator/v10"
)

func ValidateGender(field validator.FieldLevel) bool {
	gender := field.Field().String()

	switch gender {
	case "male", "female", "all":
		return true
	}

	return false
}
