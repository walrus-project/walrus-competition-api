package responseDTO

import (
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
)

type RaceOrdersResponseDTO struct {
	RaceResponseDTO
	Orders []*ordersResponseDTO.OrderResponseDTO `json:"orders"`
}
