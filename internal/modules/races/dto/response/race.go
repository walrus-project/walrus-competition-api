package responseDTO

import (
	"github.com/volatiletech/null/v8"
)

type RaceResponseDTO struct {
	Id            int      `json:"id"`
	Name          string   `json:"name"`
	Distance      int      `json:"distance"`
	SwimmingStyle string   `json:"swimming_style"`
	Gender        string   `json:"gender"`
	MinAge        null.Int `json:"min_age"`
	MaxAge        null.Int `json:"max_age"`
	ParaSwimmers  bool     `json:"para_swimmers"`
	Date          string   `json:"date"`
}
