package serializers

import (
	"context"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
	ordersSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/serializers"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/dto/response"
)

type RaceOrdersSerializer struct {
	raceSerializer  *RaceSerializer
	orderSerializer *ordersSerializers.OrderSerializer
}

func (s *RaceOrdersSerializer) Serialize(ctx context.Context, race *models.Race) *responseDTO.RaceOrdersResponseDTO {
	response := &responseDTO.RaceOrdersResponseDTO{
		RaceResponseDTO: *s.raceSerializer.Serialize(ctx, race),
		Orders:          make([]*ordersResponseDTO.OrderResponseDTO, 0, len(race.R.Orders)),
	}

	for _, order := range race.R.Orders {
		response.Orders = append(response.Orders, s.orderSerializer.Serialize(ctx, order))
	}

	return response
}

func NewRaceOrdersSerializer(raceSerializer *RaceSerializer, orderSerializer *ordersSerializers.OrderSerializer) *RaceOrdersSerializer {
	return &RaceOrdersSerializer{
		raceSerializer:  raceSerializer,
		orderSerializer: orderSerializer,
	}
}
