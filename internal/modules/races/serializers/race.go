package serializers

import (
	"context"
	"time"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/dto/response"
)

type RaceSerializer struct{}

func (s *RaceSerializer) Serialize(ctx context.Context, race *models.Race) *responseDTO.RaceResponseDTO {
	return &responseDTO.RaceResponseDTO{
		Id:            race.ID,
		Name:          race.Name,
		Distance:      race.Distance,
		SwimmingStyle: race.SwimmingStyle.String(),
		Gender:        race.Gender.String(),
		MinAge:        race.MinAge,
		MaxAge:        race.MaxAge,
		ParaSwimmers:  race.ParaSwimmers,
		Date:          race.Date.Format(time.RFC3339),
	}
}
