package services

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type RacesOrdersService struct {
	db *sql.DB
}

func (s *RacesOrdersService) FindCompetitionRaces(ctx context.Context, competitionID int) (models.RaceSlice, error) {
	races, err := models.Races(
		qm.Load(qm.Rels(models.RaceRels.Orders), models.OrderWhere.Status.NEQ(models.OrderStatusRejected)),
		models.RaceWhere.CompetitionID.EQ(null.IntFrom(competitionID)),
	).All(ctx, s.db)
	if err != nil && err != sql.ErrNoRows {
		return nil, fmt.Errorf("failed to find races orders (competition id: %d), error: %w", competitionID, err)
	}

	return races, nil
}

func NewRacesOrdersService(db *sql.DB) *RacesOrdersService {
	return &RacesOrdersService{
		db: db,
	}
}
