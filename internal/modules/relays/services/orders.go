package services

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type RelaysOrdersService struct {
	db *sql.DB
}

func (s *RelaysOrdersService) FindCompetitionRelays(ctx context.Context, competitionID int) (models.RelaySlice, error) {
	relays, err := models.Relays(
		qm.Load(qm.Rels(models.RelayRels.Orders), models.OrderWhere.Status.NEQ(models.OrderStatusRejected)),
		models.RelayWhere.CompetitionID.EQ(null.IntFrom(competitionID)),
	).All(ctx, s.db)
	if err != nil && err != sql.ErrNoRows {
		return nil, fmt.Errorf("failed to find relays orders (competition id: %d), error: %w", competitionID, err)
	}

	return relays, nil
}

func NewRelaysOrdersService(db *sql.DB) *RelaysOrdersService {
	return &RelaysOrdersService{
		db: db,
	}
}
