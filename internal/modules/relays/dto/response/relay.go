package responseDTO

type RelayResponseDTO struct {
	Id            int    `json:"id"`
	Name          string `json:"name"`
	Distance      int    `json:"distance"`
	SwimmingStyle string `json:"swimming_style"`
	Count         int    `json:"count"`
	Date          string `json:"date"`
}
