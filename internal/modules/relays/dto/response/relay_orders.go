package responseDTO

import (
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
)

type RelayOrdersResponseDTO struct {
	RelayResponseDTO
	Orders []*ordersResponseDTO.OrderResponseDTO `json:"orders"`
}
