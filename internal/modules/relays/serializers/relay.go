package serializers

import (
	"context"
	"time"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/dto/response"
)

type RelaySerializer struct{}

func (s *RelaySerializer) Serialize(ctx context.Context, relay *models.Relay) *responseDTO.RelayResponseDTO {
	return &responseDTO.RelayResponseDTO{
		Id:            relay.ID,
		Name:          relay.Name,
		Distance:      relay.Distance,
		SwimmingStyle: relay.SwimmingStyle.String(),
		Count:         relay.Count,
		Date:          relay.Date.Format(time.RFC3339),
	}
}
