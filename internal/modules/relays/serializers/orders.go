package serializers

import (
	"context"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
	ordersSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/serializers"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/dto/response"
)

type RelayOrdersSerializer struct {
	relaySerializer *RelaySerializer
	orderSerializer *ordersSerializers.OrderSerializer
}

func (s *RelayOrdersSerializer) Serialize(ctx context.Context, relay *models.Relay) *responseDTO.RelayOrdersResponseDTO {
	response := &responseDTO.RelayOrdersResponseDTO{
		RelayResponseDTO: *s.relaySerializer.Serialize(ctx, relay),
		Orders:           make([]*ordersResponseDTO.OrderResponseDTO, 0, len(relay.R.Orders)),
	}

	for _, order := range relay.R.Orders {
		response.Orders = append(response.Orders, s.orderSerializer.Serialize(ctx, order))
	}

	return response
}

func NewRelayOrdersSerializer(relaySerializer *RelaySerializer, orderSerializer *ordersSerializers.OrderSerializer) *RelayOrdersSerializer {
	return &RelayOrdersSerializer{
		relaySerializer: relaySerializer,
		orderSerializer: orderSerializer,
	}
}
