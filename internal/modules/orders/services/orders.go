package services

import (
	"context"
	"database/sql"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/boil"

	transformHelpers "gitlab.com/walrus-project/walrus-competition-api/internal/common/helpers/transform"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	competitionsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/services"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/errors"
)

type OrdersService struct {
	log                    *logrus.Entry
	db                     *sql.DB
	competitionsService    *competitionsServices.CompetitionsService
	orderMailNotifyService *OrderMailNotifyService
}

func (s *OrdersService) CreateOrder(
	ctx context.Context,
	competitionID int,
	firstName string,
	lastName string,
	middleName *string,
	paraSwimmer bool,
	clubName string,
	location string,
	birthDate string,
	gender string,
	email string,
	phone *string,
	needSkis bool,
	races []int,
	relays []int,
	cryathlons []int,
	aquathlons []int,
	additional *string,
) (*models.Order, error) {
	bd, err := transformHelpers.ToDate(birthDate)
	if err != nil {
		return nil, &errors.CreateOrderError{Code: errors.CreateOrderErrorCodeTransformBirthdayError, Message: err.Error()}
	}

	order := &models.Order{
		CompetitionID: competitionID,
		LastName:      lastName,
		FirstName:     firstName,
		MiddleName:    null.String{},
		Birthdate:     *bd,
		Gender:        transformHelpers.ToGender(gender),
		ParaSwimmer:   paraSwimmer,
		ClubName:      clubName,
		Location:      location,
		Email:         email,
		Phone:         null.String{},
		NeedSkis:      needSkis,
		Additional:    null.String{},
		Status:        models.OrderStatusNew,
		CreatedAt:     time.Now(),
	}

	if middleName != nil {
		order.MiddleName = null.StringFrom(*middleName)
	}

	if phone != nil {
		order.Phone = null.StringFrom(*phone)
	}

	if additional != nil {
		order.Additional = null.StringFrom(*additional)
	}

	competition, err := s.competitionsService.FindOne(ctx, competitionID)
	if err != nil || competition == nil {
		return nil, &errors.CreateOrderError{Code: errors.CreateOrderErrorCodeCompetitionNotFound, Message: err.Error()}
	}

	racesCount, relaysCount, cryathlonsCount, aquathlonsCount := len(races), len(relays), len(cryathlons), len(aquathlons)
	if racesCount+relaysCount+cryathlonsCount+aquathlonsCount == 0 {
		return nil, &errors.CreateOrderError{Code: errors.CreateOrderErrorCodeNoDistances}
	}

	if err := order.Insert(ctx, s.db, boil.Infer()); err != nil {
		return nil, &errors.CreateOrderError{Code: errors.CreateOrderErrorCodeInsertOrderError, Message: err.Error()}
	}

	errCh := make(chan error, 4)

	wg := sync.WaitGroup{}
	wg.Add(4)

	//	fetch selected order races
	go func() {
		defer wg.Done()

		if racesCount > 0 {
			var racesErr error
			selectedRaces, racesErr := models.Races(models.RaceWhere.ID.IN(races)).All(ctx, s.db)
			if racesErr != nil {
				errCh <- racesErr
			} else if addErr := order.AddRaces(ctx, s.db, false, selectedRaces...); addErr != nil {
				errCh <- addErr
			}
		}
	}()

	//	fetch selected order relays
	go func() {
		defer wg.Done()

		if relaysCount > 0 {
			var relaysErr error
			selectedRelays, relaysErr := models.Relays(models.RelayWhere.ID.IN(relays)).All(ctx, s.db)
			if relaysErr != nil {
				errCh <- relaysErr
			} else if addErr := order.AddRelays(ctx, s.db, false, selectedRelays...); addErr != nil {
				errCh <- addErr
			}
		}
	}()

	//	fetch selected order cryathlons
	go func() {
		defer wg.Done()

		if cryathlonsCount > 0 {
			var cryathlonsErr error
			selectedCryathlons, cryathlonsErr := models.Cryathlons(models.CryathlonWhere.ID.IN(cryathlons)).All(ctx, s.db)
			if cryathlonsErr != nil {
				errCh <- cryathlonsErr
			} else if addErr := order.AddCryathlons(ctx, s.db, false, selectedCryathlons...); addErr != nil {
				errCh <- addErr
			}
		}
	}()

	//	fetch selected order aquathlons
	go func() {
		defer wg.Done()

		if aquathlonsCount > 0 {
			var aquathlonsErr error
			selectedAquathlons, aquathlonsErr := models.Aquathlons(models.AquathlonWhere.ID.IN(aquathlons)).All(ctx, s.db)
			if aquathlonsErr != nil {
				errCh <- aquathlonsErr
			} else if addErr := order.AddAquathlons(ctx, s.db, false, selectedAquathlons...); addErr != nil {
				errCh <- addErr
			}
		}
	}()

	wg.Wait()

	for i := 0; i < 4; i++ {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case addErr := <-errCh:
			return nil, addErr
		default:
		}
	}

	if err := s.orderMailNotifyService.SendNotify(competition, order); err != nil {
		s.log.WithError(err).Error("failed to send order notify mail")
	}

	return order, nil
}

func NewOrdersService(l *logrus.Entry, db *sql.DB, competitionsService *competitionsServices.CompetitionsService, orderMailNotifyService *OrderMailNotifyService) *OrdersService {
	return &OrdersService{
		log:                    l,
		db:                     db,
		competitionsService:    competitionsService,
		orderMailNotifyService: orderMailNotifyService,
	}
}
