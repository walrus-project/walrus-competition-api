package services

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type CompetitionsOrdersService struct {
	db *sql.DB
}

func (s *CompetitionsOrdersService) FindCompetitionOrders(ctx context.Context, competitionID int) (models.OrderSlice, error) {
	orders, err := models.Orders(
		models.OrderWhere.CompetitionID.EQ(competitionID),
		models.OrderWhere.Status.NEQ(models.OrderStatusRejected),
		qm.OrderBy(models.OrderColumns.CreatedAt),
	).All(ctx, s.db)
	if err != nil {
		return nil, fmt.Errorf("failed to find competition orders (competition id: %d), error: %w", competitionID, err)
	}

	return orders, nil
}

func NewCompetitionsOrdersService(db *sql.DB) *CompetitionsOrdersService {
	return &CompetitionsOrdersService{
		db: db,
	}
}
