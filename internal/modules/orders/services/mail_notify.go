package services

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"html/template"
	"net/smtp"

	mailConfig "gitlab.com/walrus-project/walrus-competition-api/configs/mail"
	ordersConfig "gitlab.com/walrus-project/walrus-competition-api/configs/orders"
	mailHelpers "gitlab.com/walrus-project/walrus-competition-api/internal/common/helpers/mail"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type OrderMailNotifyService struct {
	mailConf     *mailConfig.Config
	ordersConfig *ordersConfig.Config
}

type OrderMailDataParticipant struct {
	ParaSwimmer bool
	NeedSkis    bool
	LastName    string
	FirstName   string
	MiddleName  string
	ClubName    string
	Location    string
	Birthdate   string
	Age         int
	Gender      string
	Email       string
	Phone       string
}

type OrderMailDataRaceItem struct {
	Name          string
	ParaSwimmers  bool
	Distance      int
	SwimmingStyle string
	Gender        string
	AgeGroup      string
	Date          string
}

type OrderMailDataRelayItem struct {
	Name          string
	Distance      int
	SwimmingStyle string
	Count         int
	Date          string
}

type OrderMailDataCryathlonItem struct {
	Name             string
	RunDistance      int
	SkiDistance      int
	WaterDistance    int
	BarefootDistance int
	Gender           string
	Date             string
}

type OrderMailDataAquathlonITem struct {
	Name          string
	RunDistance   int
	WaterDistance int
	Gender        string
	Date          string
}

type OrderMailData struct {
	CompetitionName string
	Participant     OrderMailDataParticipant
	Races           []OrderMailDataRaceItem
	Relays          []OrderMailDataRelayItem
	Cryathlons      []OrderMailDataCryathlonItem
	Aquathlons      []OrderMailDataAquathlonITem
	Additional      string
}

func (s *OrderMailNotifyService) parseTemplate(body *bytes.Buffer, competition *models.Competition, order *models.Order) error {
	tpl, err := template.ParseFiles(s.ordersConfig.TemplateFilePath)
	if err != nil {
		return fmt.Errorf("failed to parse order template file: %w", err)
	}

	data := OrderMailData{
		CompetitionName: competition.Name,
		Participant: OrderMailDataParticipant{
			ParaSwimmer: order.ParaSwimmer,
			NeedSkis:    order.NeedSkis,
			LastName:    order.LastName,
			FirstName:   order.FirstName,
			MiddleName:  order.MiddleName.String,
			ClubName:    order.ClubName,
			Location:    order.Location,
			Birthdate:   mailHelpers.FormatDate(order.Birthdate),
			Age:         mailHelpers.YearsFromTime(order.Birthdate),
			Gender:      mailHelpers.GenderText(order.Gender),
			Email:       order.Email,
			Phone:       order.Phone.String,
		},
		Additional: order.Additional.String,
		Races:      make([]OrderMailDataRaceItem, 0, len(order.R.Races)),
		Relays:     make([]OrderMailDataRelayItem, 0, len(order.R.Relays)),
		Cryathlons: make([]OrderMailDataCryathlonItem, 0, len(order.R.Cryathlons)),
		Aquathlons: make([]OrderMailDataAquathlonITem, 0, len(order.R.Aquathlons)),
	}

	for _, race := range order.R.Races {
		data.Races = append(data.Races, OrderMailDataRaceItem{
			Name:          race.Name,
			ParaSwimmers:  race.ParaSwimmers,
			Distance:      race.Distance,
			SwimmingStyle: mailHelpers.SwimmingStyleText(race.SwimmingStyle),
			Gender:        mailHelpers.GenderText(race.Gender),
			AgeGroup:      mailHelpers.AgeGroupText(race.MinAge, race.MaxAge),
			Date:          mailHelpers.FormatDate(race.Date),
		})
	}

	for _, relay := range order.R.Relays {
		data.Relays = append(data.Relays, OrderMailDataRelayItem{
			Name:          relay.Name,
			Distance:      relay.Distance,
			SwimmingStyle: mailHelpers.SwimmingStyleText(relay.SwimmingStyle),
			Count:         relay.Count,
			Date:          mailHelpers.FormatDate(relay.Date),
		})
	}

	for _, cryathlon := range order.R.Cryathlons {
		data.Cryathlons = append(data.Cryathlons, OrderMailDataCryathlonItem{
			Name:             cryathlon.Name,
			RunDistance:      cryathlon.RunDistance,
			SkiDistance:      cryathlon.SkiDistance,
			WaterDistance:    cryathlon.WaterDistance,
			BarefootDistance: cryathlon.BarefootDistance,
			Gender:           mailHelpers.GenderText(cryathlon.Gender),
			Date:             mailHelpers.FormatDate(cryathlon.Date),
		})
	}

	for _, aquathlon := range order.R.Aquathlons {
		data.Aquathlons = append(data.Aquathlons, OrderMailDataAquathlonITem{
			Name:          aquathlon.Name,
			RunDistance:   aquathlon.RunDistance,
			WaterDistance: aquathlon.WaterDistance,
			Gender:        mailHelpers.GenderText(aquathlon.Gender),
			Date:          mailHelpers.FormatDate(aquathlon.Date),
		})
	}

	if err = tpl.Execute(body, data); err != nil {
		return fmt.Errorf("failed to execute order template: %w", err)
	}

	return nil
}

func (s *OrderMailNotifyService) SendNotify(competition *models.Competition, order *models.Order) error {
	conn, err := tls.Dial("tcp", s.mailConf.Address(), s.mailConf.TLSConfig())
	if err != nil {
		return fmt.Errorf("failed to connect to mail server: %w", err)
	}

	c, err := smtp.NewClient(conn, s.mailConf.Host)
	if err != nil {
		return fmt.Errorf("failed to create smtp client: %w", err)
	}
	defer c.Quit()

	if err = c.Auth(s.mailConf.Auth()); err != nil {
		return fmt.Errorf("failed to authenticate: %w", err)
	}

	if err = c.Mail(s.ordersConfig.NotifyFrom.Address); err != nil {
		return fmt.Errorf("failed to send mail command: %w", err)
	}

	for _, addr := range s.ordersConfig.NotifyTo {
		if err = c.Rcpt(addr.Address); err != nil {
			return fmt.Errorf("failed to send rcpt command (address: %s), error: %w", addr.Address, err)
		}
	}

	w, err := c.Data()
	if err != nil {
		return fmt.Errorf("faield to send data command: %w", err)
	}

	var body bytes.Buffer
	from := fmt.Sprintf("From: %s", s.ordersConfig.NotifyFromAddress())
	to := fmt.Sprintf("To: %s", s.ordersConfig.NotifyToAddresses())
	subject := fmt.Sprintf("Subject: %s", s.ordersConfig.NotifySubject)
	headers := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";"

	if _, err = w.Write([]byte(fmt.Sprintf("%s\n%s\n%s\n%s\n\n", from, to, subject, headers))); err != nil {
		return fmt.Errorf("faield to write mail headers: %w", err)
	}

	if err := s.parseTemplate(&body, competition, order); err != nil {
		return fmt.Errorf("failed to parse mail template: %w", err)
	}

	if _, err = w.Write(body.Bytes()); err != nil {
		return fmt.Errorf("faield to write mail body: %w", err)
	}

	if err = w.Close(); err != nil {
		return fmt.Errorf("faield to close mail writer: %w", err)
	}

	return nil
}

func NewOrderMailNotifyService(mailConf *mailConfig.Config, ordersConf *ordersConfig.Config) *OrderMailNotifyService {
	return &OrderMailNotifyService{
		mailConf:     mailConf,
		ordersConfig: ordersConf,
	}
}
