package serializers

import (
	"context"
	"time"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
)

type OrderSerializer struct{}

func (s *OrderSerializer) Serialize(ctx context.Context, order *models.Order) *responseDTO.OrderResponseDTO {
	return &responseDTO.OrderResponseDTO{
		Id:         order.ID,
		LastName:   order.LastName,
		FirstName:  order.FirstName,
		MiddleName: order.MiddleName.Ptr(),
		BirthDate:  order.Birthdate.Format(time.RFC3339),
		ClubName:   order.ClubName,
		Location:   order.Location,
		Status:     order.Status.String(),
	}
}
