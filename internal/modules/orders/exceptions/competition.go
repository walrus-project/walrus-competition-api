package exceptions

import (
	"net/http"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
)

const (
	findCompetitionOrdersErrorCode = "find_competition_orders_error"
)

func FindCompetitionOrdersException(w http.ResponseWriter, err error) {
	findException := httpExceptions.CreateInternalServerErrorException[any, httpExceptions.HttpExceptionResponse](findCompetitionOrdersErrorCode, err.Error(), err)

	findException.Error(w, findException.GetResponse())
}
