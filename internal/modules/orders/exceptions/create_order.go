package exceptions

import (
	"net/http"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
	ordersErrors "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/errors"
)

const (
	createOrderUnknownErrorCode = "create_order_unknown_error"
)

func CreateOrderException(w http.ResponseWriter, err ordersErrors.CreateOrderError) {
	createException := httpExceptions.CreateBadRequestException[any, httpExceptions.HttpExceptionResponse](string(err.Code), err.Error(), err)

	createException.Error(w, createException.GetResponse())
}

func CreateOrderUnknownException(w http.ResponseWriter, err error) {
	createUnknownException := httpExceptions.CreateInternalServerErrorException[any, httpExceptions.HttpExceptionResponse](createOrderUnknownErrorCode, err.Error(), err)

	createUnknownException.Error(w, createUnknownException.GetResponse())
}
