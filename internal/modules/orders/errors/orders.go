package errors

type CreateOrderErrorCode string

const (
	CreateOrderErrorCodeTransformBirthdayError CreateOrderErrorCode = "transform_birthday_error"
	CreateOrderErrorCodeCompetitionNotFound    CreateOrderErrorCode = "competition_not_found"
	CreateOrderErrorCodeNoDistances            CreateOrderErrorCode = "no_distances"
	CreateOrderErrorCodeInsertOrderError       CreateOrderErrorCode = "insert_order_error"
)

type CreateOrderError struct {
	Code    CreateOrderErrorCode
	Message string
}

func (e *CreateOrderError) Error() string {
	return e.Message
}
