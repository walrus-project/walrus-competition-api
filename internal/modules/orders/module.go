package orders

import (
	"database/sql"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"

	mailConfig "gitlab.com/walrus-project/walrus-competition-api/configs/mail"
	ordersConfig "gitlab.com/walrus-project/walrus-competition-api/configs/orders"
	competitionsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/services"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/controllers"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/serializers"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/services"
)

func Init(
	r chi.Router,
	l *logrus.Entry,
	db *sql.DB,
	validate *validator.Validate,
	competitionsService *competitionsServices.CompetitionsService,
	orderSerializer *serializers.OrderSerializer,
	mailConf *mailConfig.Config,
	ordersConf *ordersConfig.Config,
) error {
	//	services
	competitionsOrdersService := services.NewCompetitionsOrdersService(db)
	orderMailNotifyService := services.NewOrderMailNotifyService(mailConf, ordersConf)
	ordersService := services.NewOrdersService(l, db, competitionsService, orderMailNotifyService)

	//	controllers
	ordersController := controllers.NewOrdersController(db, validate, competitionsOrdersService, ordersService, orderSerializer)

	r.Route("/orders", func(sr chi.Router) {
		sr.Post("/", http.HandlerFunc(ordersController.CreateOrder))
		sr.Get("/competition/{competitionID}", http.HandlerFunc(ordersController.GetCompetitionOrders))
	})

	return nil
}
