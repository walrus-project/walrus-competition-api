package responseDTO

type OrderResponseDTO struct {
	Id         int     `json:"id"`
	LastName   string  `json:"last_name"`
	FirstName  string  `json:"first_name"`
	MiddleName *string `json:"middle_name"`
	BirthDate  string  `json:"birthdate"`
	ClubName   string  `json:"club_name"`
	Location   string  `json:"location"`
	Status     string  `json:"status"`
}
