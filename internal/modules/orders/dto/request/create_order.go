package requestDTO

type CreateOrderRequestDTO struct {
	CompetitionID int     `json:"competition_id" validate:"required"`
	FirstName     string  `json:"first_name" validate:"required"`
	LastName      string  `json:"last_name" validate:"required"`
	MiddleName    *string `json:"middle_name"`
	ParaSwimmer   bool    `json:"para_swimmer"`
	ClubName      string  `json:"club_name" validate:"required"`
	Location      string  `json:"location" validate:"required"`
	BirthDate     string  `json:"birthdate" validate:"required,date"`
	Gender        string  `json:"gender" validate:"required,gender"`
	Email         string  `json:"email" validate:"required,email"`
	Phone         *string `json:"phone"`
	NeedSkis      bool    `json:"need_skis"`
	Races         []int   `json:"races" validate:"dive"`
	Relays        []int   `json:"relays" validate:"dive"`
	Cryathlons    []int   `json:"cryathlons" validate:"dive"`
	Aquathlons    []int   `json:"aquathlons" validate:"dive"`
	Additional    *string `json:"additional"`
}
