package controllers

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"

	commonExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/exceptions"
	"gitlab.com/walrus-project/walrus-competition-api/internal/common/serializers"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	requestDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/request"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
	ordersErrors "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/errors"
	ordersExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/exceptions"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/services"
)

type OrdersController struct {
	db                        *sql.DB
	validate                  *validator.Validate
	competitionsOrdersService *services.CompetitionsOrdersService
	ordersService             *services.OrdersService
	orderSerializer           serializers.BaseSerializer[*models.Order, *responseDTO.OrderResponseDTO]
}

func (c *OrdersController) CreateOrder(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var body requestDTO.CreateOrderRequestDTO
	defer r.Body.Close()

	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		commonExceptions.CreateDecodeRequestBodyException(w, err)

		return
	}

	if err := c.validate.Struct(body); err != nil {
		commonExceptions.CreateRequestBodyValidationException(w, err)

		return

	}

	order, err := c.ordersService.CreateOrder(
		ctx,
		body.CompetitionID,
		body.FirstName,
		body.LastName,
		body.MiddleName,
		body.ParaSwimmer,
		body.ClubName,
		body.Location,
		body.BirthDate,
		body.Gender,
		body.Email,
		body.Phone,
		body.NeedSkis,
		body.Races,
		body.Relays,
		body.Cryathlons,
		body.Aquathlons,
		body.Additional,
	)
	if err != nil {
		if createOrderError, ok := err.(*ordersErrors.CreateOrderError); ok {
			ordersExceptions.CreateOrderException(w, *createOrderError)

			return
		}

		ordersExceptions.CreateOrderUnknownException(w, err)

		return
	}

	json.NewEncoder(w).Encode(c.orderSerializer.Serialize(ctx, order))
}

func (c *OrdersController) GetCompetitionOrders(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	competitionID, err := strconv.Atoi(chi.URLParam(r, "competitionID"))
	if err != nil {
		commonExceptions.InvalidIDParamInURLException(w, err)

		return
	}

	orders, err := c.competitionsOrdersService.FindCompetitionOrders(ctx, competitionID)
	if err != nil {
		ordersExceptions.FindCompetitionOrdersException(w, err)

		return
	}

	response := make([]responseDTO.OrderResponseDTO, 0, len(orders))
	for _, order := range orders {
		response = append(response, *c.orderSerializer.Serialize(ctx, order))
	}

	json.NewEncoder(w).Encode(response)
}

func NewOrdersController(
	db *sql.DB,
	validate *validator.Validate,
	competitionsOrdersService *services.CompetitionsOrdersService,
	ordersService *services.OrdersService,
	orderSerializer serializers.BaseSerializer[*models.Order, *responseDTO.OrderResponseDTO],
) *OrdersController {
	return &OrdersController{
		db:                        db,
		validate:                  validate,
		competitionsOrdersService: competitionsOrdersService,
		ordersService:             ordersService,
		orderSerializer:           orderSerializer,
	}
}
