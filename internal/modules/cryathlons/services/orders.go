package services

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type CryathlonsOrdersService struct {
	db *sql.DB
}

func (s *CryathlonsOrdersService) FindCompetitionCryathlons(ctx context.Context, competitionID int) (models.CryathlonSlice, error) {
	cryathlons, err := models.Cryathlons(
		qm.Load(qm.Rels(models.CryathlonRels.Orders), models.OrderWhere.Status.NEQ(models.OrderStatusRejected)),
		models.CryathlonWhere.CompetitionID.EQ(null.IntFrom(competitionID)),
	).All(ctx, s.db)
	if err != nil && err != sql.ErrNoRows {
		return nil, fmt.Errorf("failed to find cryathlons orders (competition id: %d), error: %w", competitionID, err)
	}

	return cryathlons, nil
}

func NewCryathlonsOrdersService(db *sql.DB) *CryathlonsOrdersService {
	return &CryathlonsOrdersService{
		db: db,
	}
}
