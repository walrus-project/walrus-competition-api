package responseDTO

import (
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
)

type CryathlonOrdersResponseDTO struct {
	CryathlonResponseDTO
	Orders []*ordersResponseDTO.OrderResponseDTO `json:"orders"`
}
