package responseDTO

type CryathlonResponseDTO struct {
	Id               int    `json:"id"`
	Name             string `json:"name"`
	RunDistance      int    `json:"run_distance"`
	SkiDistance      int    `json:"ski_distance"`
	WaterDistance    int    `json:"water_distance"`
	BarefootDistance int    `json:"barefoot_distance"`
	Gender           string `json:"gender"`
	Date             string `json:"date"`
}
