package serializers

import (
	"context"
	"time"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/dto/response"
)

type CryathlonSerializer struct{}

func (s *CryathlonSerializer) Serialize(ctx context.Context, cryathlon *models.Cryathlon) *responseDTO.CryathlonResponseDTO {
	return &responseDTO.CryathlonResponseDTO{
		Id:               cryathlon.ID,
		Name:             cryathlon.Name,
		RunDistance:      cryathlon.RunDistance,
		SkiDistance:      cryathlon.SkiDistance,
		WaterDistance:    cryathlon.WaterDistance,
		BarefootDistance: cryathlon.BarefootDistance,
		Gender:           cryathlon.Gender.String(),
		Date:             cryathlon.Date.Format(time.RFC3339),
	}
}
