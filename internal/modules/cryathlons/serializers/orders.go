package serializers

import (
	"context"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/dto/response"
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
	ordersSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/serializers"
)

type CryathlonOrdersSerializer struct {
	cryathlonSerializer *CryathlonSerializer
	orderSerializer     *ordersSerializers.OrderSerializer
}

func (s *CryathlonOrdersSerializer) Serialize(ctx context.Context, cryathlon *models.Cryathlon) *responseDTO.CryathlonOrdersResponseDTO {
	response := &responseDTO.CryathlonOrdersResponseDTO{
		CryathlonResponseDTO: *s.cryathlonSerializer.Serialize(ctx, cryathlon),
		Orders:               make([]*ordersResponseDTO.OrderResponseDTO, 0, len(cryathlon.R.Orders)),
	}

	for _, order := range cryathlon.R.Orders {
		response.Orders = append(response.Orders, s.orderSerializer.Serialize(ctx, order))
	}

	return response
}

func NewCyrathlonOrdersSerializer(cryathlonSerializer *CryathlonSerializer, orderSerializer *ordersSerializers.OrderSerializer) *CryathlonOrdersSerializer {
	return &CryathlonOrdersSerializer{
		cryathlonSerializer: cryathlonSerializer,
		orderSerializer:     orderSerializer,
	}
}
