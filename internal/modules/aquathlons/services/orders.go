package services

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/null/v8"
	"github.com/volatiletech/sqlboiler/v4/queries/qm"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type AquathlonsOrdersService struct {
	db *sql.DB
}

func (s *AquathlonsOrdersService) FindCompetitionAquathlons(ctx context.Context, competitionID int) (models.AquathlonSlice, error) {
	aquathlons, err := models.Aquathlons(
		qm.Load(qm.Rels(models.AquathlonRels.Orders), models.OrderWhere.Status.NEQ(models.OrderStatusRejected)),
		models.AquathlonWhere.CompetitionID.EQ(null.IntFrom(competitionID)),
	).All(ctx, s.db)
	if err != nil && err != sql.ErrNoRows {
		return nil, fmt.Errorf("failed to find aquathlons orders (competition id: %d), error: %w", competitionID, err)
	}

	return aquathlons, nil
}

func NewAquathlonsOrdersService(db *sql.DB) *AquathlonsOrdersService {
	return &AquathlonsOrdersService{
		db: db,
	}
}
