package responseDTO

import (
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
)

type AquathlonOrdersResponseDTO struct {
	AquathlonResponseDTO
	Orders []*ordersResponseDTO.OrderResponseDTO `json:"orders"`
}
