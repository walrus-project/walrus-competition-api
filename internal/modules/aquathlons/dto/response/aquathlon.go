package responseDTO

type AquathlonResponseDTO struct {
	Id            int    `json:"id"`
	Name          string `json:"name"`
	RunDistance   int    `json:"run_distance"`
	WaterDistance int    `json:"water_distance"`
	Gender        string `json:"gender"`
	Date          string `json:"date"`
}
