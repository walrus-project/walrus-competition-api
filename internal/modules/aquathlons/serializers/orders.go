package serializers

import (
	"context"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/dto/response"
	ordersResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/dto/response"
	ordersSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/serializers"
)

type AquathlonOrdersSerializer struct {
	aquathlonSerializer *AquathlonSerializer
	orderSerializer     *ordersSerializers.OrderSerializer
}

func (s *AquathlonOrdersSerializer) Serialize(ctx context.Context, aquathlon *models.Aquathlon) *responseDTO.AquathlonOrdersResponseDTO {
	response := &responseDTO.AquathlonOrdersResponseDTO{
		AquathlonResponseDTO: *s.aquathlonSerializer.Serialize(ctx, aquathlon),
		Orders:               make([]*ordersResponseDTO.OrderResponseDTO, 0, len(aquathlon.R.Orders)),
	}

	for _, order := range aquathlon.R.Orders {
		response.Orders = append(response.Orders, s.orderSerializer.Serialize(ctx, order))
	}

	return response
}

func NewAquathlonOrdersSerializer(aquathlonSerializer *AquathlonSerializer, orderSerializer *ordersSerializers.OrderSerializer) *AquathlonOrdersSerializer {
	return &AquathlonOrdersSerializer{
		aquathlonSerializer: aquathlonSerializer,
		orderSerializer:     orderSerializer,
	}
}
