package serializers

import (
	"context"
	"time"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/dto/response"
)

type AquathlonSerializer struct{}

func (s *AquathlonSerializer) Serialize(ctx context.Context, aquathlon *models.Aquathlon) *responseDTO.AquathlonResponseDTO {
	return &responseDTO.AquathlonResponseDTO{
		Id:            aquathlon.ID,
		Name:          aquathlon.Name,
		RunDistance:   aquathlon.RunDistance,
		WaterDistance: aquathlon.WaterDistance,
		Gender:        aquathlon.Gender.String(),
		Date:          aquathlon.Date.Format(time.RFC3339),
	}
}
