package services

import (
	"context"
	"database/sql"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	aquathlonsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/services"
	cryathlonsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/services"
	racesServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/services"
	relaysServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/services"
)

type CompetitionsProtocolsService struct {
	db                      *sql.DB
	racesOrdersService      *racesServices.RacesOrdersService
	relaysOrdersService     *relaysServices.RelaysOrdersService
	aquathlonsOrdersService *aquathlonsServices.AquathlonsOrdersService
	cryathlonsOrdersService *cryathlonsServices.CryathlonsOrdersService
}

type CompetitionProtocols struct {
	Competition *models.Competition
	Races       models.RaceSlice
	Relays      models.RelaySlice
	Aquathlons  models.AquathlonSlice
	Cryathlons  models.CryathlonSlice
}

func (s *CompetitionsProtocolsService) FindCompetitionProtocols(ctx context.Context, id int) (*CompetitionProtocols, error) {
	racesCh := make(chan models.RaceSlice, 1)
	relaysCh := make(chan models.RelaySlice, 1)
	aquathlonsCh := make(chan models.AquathlonSlice, 1)
	cryathlonsCh := make(chan models.CryathlonSlice, 1)
	errCh := make(chan error, 4)
	competitionProtocols := &CompetitionProtocols{}

	go func() {
		races, err := s.racesOrdersService.FindCompetitionRaces(ctx, id)
		if err != nil {
			errCh <- err
		} else {
			racesCh <- races
		}
	}()

	go func() {
		relays, err := s.relaysOrdersService.FindCompetitionRelays(ctx, id)
		if err != nil {
			errCh <- err
		} else {
			relaysCh <- relays
		}
	}()

	go func() {
		aquathlons, err := s.aquathlonsOrdersService.FindCompetitionAquathlons(ctx, id)
		if err != nil {
			errCh <- err
		} else {
			aquathlonsCh <- aquathlons
		}
	}()

	go func() {
		cryathlons, err := s.cryathlonsOrdersService.FindCompetitionCryathlons(ctx, id)
		if err != nil {
			errCh <- err
		} else {
			cryathlonsCh <- cryathlons
		}
	}()

	for i := 0; i < 4; i++ {
		select {
		case <-ctx.Done():
			return nil, ctx.Err()
		case err := <-errCh:
			return nil, err
		case races := <-racesCh:
			competitionProtocols.Races = races
		case relays := <-relaysCh:
			competitionProtocols.Relays = relays
		case aquathlons := <-aquathlonsCh:
			competitionProtocols.Aquathlons = aquathlons
		case cryathlons := <-cryathlonsCh:
			competitionProtocols.Cryathlons = cryathlons
		}
	}

	return competitionProtocols, nil
}

func NewCompetitionsProtocolsService(
	db *sql.DB,
	racesOrdersService *racesServices.RacesOrdersService,
	relaysOrdersService *relaysServices.RelaysOrdersService,
	aquathlonsOrdersService *aquathlonsServices.AquathlonsOrdersService,
	cryathlonsOrdersService *cryathlonsServices.CryathlonsOrdersService,
) *CompetitionsProtocolsService {
	return &CompetitionsProtocolsService{
		db:                      db,
		racesOrdersService:      racesOrdersService,
		relaysOrdersService:     relaysOrdersService,
		aquathlonsOrdersService: aquathlonsOrdersService,
		cryathlonsOrdersService: cryathlonsOrdersService,
	}
}
