package services

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/volatiletech/sqlboiler/v4/queries/qm"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
)

type CompetitionsService struct {
	db *sql.DB
}

func (s *CompetitionsService) FindOne(ctx context.Context, id int) (*models.Competition, error) {
	competition, err := models.Competitions(
		qm.Load(qm.Rels(models.CompetitionRels.Races)),
		qm.Load(qm.Rels(models.CompetitionRels.Relays)),
		qm.Load(qm.Rels(models.CompetitionRels.Aquathlons)),
		qm.Load(qm.Rels(models.CompetitionRels.Cryathlons)),
		models.CompetitionWhere.ID.EQ(id),
	).One(ctx, s.db)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		return nil, fmt.Errorf("failed to find competition (id: %d), error: %w", id, err)
	}

	return competition, nil
}

func (s *CompetitionsService) FindUpcoming(ctx context.Context) (models.CompetitionSlice, error) {
	competitions, err := models.Competitions(
		models.CompetitionWhere.StartDate.GTE(time.Now()),
		qm.OrderBy(models.CompetitionColumns.StartDate),
	).All(ctx, s.db)
	if err != nil {
		return nil, fmt.Errorf("failed to find upcoming competitions: %w", err)
	}

	return competitions, nil
}

func NewCompetitionsService(db *sql.DB) *CompetitionsService {
	return &CompetitionsService{
		db: db,
	}
}
