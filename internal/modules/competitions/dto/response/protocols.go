package responseDTO

import (
	aquathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/dto/response"
	cryathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/dto/response"
	racesResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/dto/response"
	relaysResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/dto/response"
)

type CompetitionProtocolsDTO struct {
	CompetitionResponseDTO
	Races      []*racesResponseDTO.RaceOrdersResponseDTO           `json:"races"`
	Relays     []*relaysResponseDTO.RelayOrdersResponseDTO         `json:"relays"`
	Aquathlons []*aquathlonsResponseDTO.AquathlonOrdersResponseDTO `json:"aquathlons"`
	Cryathlons []*cryathlonsResponseDTO.CryathlonOrdersResponseDTO `json:"cryathlons"`
}
