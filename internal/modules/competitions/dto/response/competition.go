package responseDTO

import (
	aquathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/dto/response"
	cryathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/dto/response"
	racesResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/dto/response"
	relaysResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/dto/response"
)

type CompetitionResponseDTO struct {
	Id          int                                           `json:"id"`
	Name        string                                        `json:"name"`
	Description string                                        `json:"description"`
	StartDate   string                                        `json:"start_date"`
	EndDate     string                                        `json:"end_date"`
	Races       []*racesResponseDTO.RaceResponseDTO           `json:"races"`
	Relays      []*relaysResponseDTO.RelayResponseDTO         `json:"relays"`
	Aquathlons  []*aquathlonsResponseDTO.AquathlonResponseDTO `json:"aquathlons"`
	Cryathlons  []*cryathlonsResponseDTO.CryathlonResponseDTO `json:"cryathlons"`
}
