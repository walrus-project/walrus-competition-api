package controllers

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	commonExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/exceptions"
	"gitlab.com/walrus-project/walrus-competition-api/internal/common/serializers"
	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/dto/response"
	competitionsExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/exceptions"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/services"
)

type CompetitionsController struct {
	db                             *sql.DB
	competitionsService            *services.CompetitionsService
	competitionsProtocolsService   *services.CompetitionsProtocolsService
	competitionSerializer          serializers.BaseSerializer[*models.Competition, *responseDTO.CompetitionResponseDTO]
	competitionProtocolsSerializer serializers.BaseSerializer[*services.CompetitionProtocols, *responseDTO.CompetitionProtocolsDTO]
}

func (c *CompetitionsController) GetCompetition(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		commonExceptions.InvalidIDParamInURLException(w, err)

		return
	}

	competition, err := c.competitionsService.FindOne(ctx, id)
	if err != nil {
		competitionsExceptions.FindCompetitionException(w, err)

		return
	}

	if competition == nil {
		commonExceptions.ObjectNotFoundException(w)

		return
	}

	json.NewEncoder(w).Encode(c.competitionSerializer.Serialize(ctx, competition))
}

func (c *CompetitionsController) GetCompetitionProtocols(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		commonExceptions.InvalidIDParamInURLException(w, err)

		return
	}

	competition, err := c.competitionsService.FindOne(ctx, id)
	if err != nil || competition == nil {
		commonExceptions.ObjectNotFoundException(w)

		return
	}

	protocols, err := c.competitionsProtocolsService.FindCompetitionProtocols(ctx, id)
	if err != nil {
		competitionsExceptions.FindProtocolsException(w, err)

		return
	}

	protocols.Competition = competition

	json.NewEncoder(w).Encode(c.competitionProtocolsSerializer.Serialize(ctx, protocols))
}

func (c *CompetitionsController) GetUpcomingCompetitions(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	competitions, err := c.competitionsService.FindUpcoming(ctx)
	if err != nil {
		competitionsExceptions.FindCompetitionsError(w, err)

		return
	}

	json.NewEncoder(w).Encode(competitions)
}

func NewCompetitionsController(
	db *sql.DB,
	competitionsService *services.CompetitionsService,
	competitionsProtocolsService *services.CompetitionsProtocolsService,
	competitionSerializer serializers.BaseSerializer[*models.Competition, *responseDTO.CompetitionResponseDTO],
	competitionProtocolsSerializer serializers.BaseSerializer[*services.CompetitionProtocols, *responseDTO.CompetitionProtocolsDTO],
) *CompetitionsController {
	return &CompetitionsController{
		db:                             db,
		competitionsService:            competitionsService,
		competitionsProtocolsService:   competitionsProtocolsService,
		competitionSerializer:          competitionSerializer,
		competitionProtocolsSerializer: competitionProtocolsSerializer,
	}
}
