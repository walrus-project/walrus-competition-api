package exceptions

import (
	"net/http"

	httpExceptions "gitlab.com/walrus-project/walrus-competition-api/internal/common/http/exceptions"
)

const (
	findCompetitionsErrorCode = "find_competitions_error"
	findCompetitionErrorCode  = "find_competition_error"
	findProtocolsErrorCode    = "find_protocols_error"
)

func FindCompetitionsError(w http.ResponseWriter, err error) {
	findException := httpExceptions.CreateInternalServerErrorException[any, httpExceptions.HttpExceptionResponse](findCompetitionsErrorCode, err.Error(), err)

	findException.Error(w, findException.GetResponse())
}

func FindCompetitionException(w http.ResponseWriter, err error) {
	findException := httpExceptions.CreateInternalServerErrorException[any, httpExceptions.HttpExceptionResponse](findCompetitionErrorCode, err.Error(), err)

	findException.Error(w, findException.GetResponse())
}

func FindProtocolsException(w http.ResponseWriter, err error) {
	findException := httpExceptions.CreateInternalServerErrorException[any, httpExceptions.HttpExceptionResponse](findProtocolsErrorCode, err.Error(), err)

	findException.Error(w, findException.GetResponse())
}
