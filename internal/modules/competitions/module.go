package competitions

import (
	"database/sql"
	"net/http"

	"github.com/go-chi/chi/v5"

	aquathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/serializers"
	aquathlonsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/services"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/controllers"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/serializers"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/services"
	cryathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/serializers"
	cryathlonsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/services"
	racesSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/serializers"
	racesServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/services"
	relaysSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/serializers"
	relaysServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/services"
)

func Init(
	r chi.Router,
	db *sql.DB,
	competitionsService *services.CompetitionsService,
	racesOrdersService *racesServices.RacesOrdersService,
	relaysOrderService *relaysServices.RelaysOrdersService,
	aquathlonsOrdersService *aquathlonsServices.AquathlonsOrdersService,
	cryathlonsOrdersService *cryathlonsServices.CryathlonsOrdersService,
	raceSerializers *racesSerializers.RaceSerializer,
	relaySerializer *relaysSerializers.RelaySerializer,
	aquathlonSerializer *aquathlonsSerializers.AquathlonSerializer,
	cryathlonSerializer *cryathlonsSerializers.CryathlonSerializer,
	raceOrdersSerializer *racesSerializers.RaceOrdersSerializer,
	relaysOrdersSerializer *relaysSerializers.RelayOrdersSerializer,
	cryathlonOrdersSerializer *cryathlonsSerializers.CryathlonOrdersSerializer,
	aquathlonOrdersSerializer *aquathlonsSerializers.AquathlonOrdersSerializer,
) error {
	//	services
	competitionsProtocolsService := services.NewCompetitionsProtocolsService(db, racesOrdersService, relaysOrderService, aquathlonsOrdersService, cryathlonsOrdersService)

	// serializers
	competitionSerializer := serializers.NewCompetitionSerializer(raceSerializers, relaySerializer, aquathlonSerializer, cryathlonSerializer)
	competitionProtocolsSerializer := serializers.NewCompetitionProtocolsSerializer(
		competitionSerializer,
		raceOrdersSerializer,
		relaysOrdersSerializer,
		cryathlonOrdersSerializer,
		aquathlonOrdersSerializer,
	)

	//	controllers
	competitionsController := controllers.NewCompetitionsController(
		db,
		competitionsService,
		competitionsProtocolsService,
		competitionSerializer,
		competitionProtocolsSerializer,
	)

	r.Route("/competitions", func(sr chi.Router) {
		sr.Get("/{id}", http.HandlerFunc(competitionsController.GetCompetition))
		sr.Get("/protocols/{id}", http.HandlerFunc(competitionsController.GetCompetitionProtocols))
		sr.Get("/upcoming", http.HandlerFunc(competitionsController.GetUpcomingCompetitions))
	})

	return nil
}
