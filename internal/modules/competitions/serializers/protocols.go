package serializers

import (
	"context"

	aquathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/dto/response"
	aquathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/serializers"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/dto/response"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/services"
	cryathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/dto/response"
	cryathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/serializers"
	racesResponstDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/dto/response"
	racesSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/serializers"
	relaysResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/dto/response"
	relaysSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/serializers"
)

type CompetitionProtocolsSerializer struct {
	competitionSerializer     *CompetitionSerializer
	raceOrdersSerializer      *racesSerializers.RaceOrdersSerializer
	relaysOrdersSerializer    *relaysSerializers.RelayOrdersSerializer
	cryathlonOrdersSerializer *cryathlonsSerializers.CryathlonOrdersSerializer
	aquathlonOrdersSerializer *aquathlonsSerializers.AquathlonOrdersSerializer
}

func (s *CompetitionProtocolsSerializer) Serialize(ctx context.Context, protocols *services.CompetitionProtocols) *responseDTO.CompetitionProtocolsDTO {
	response := &responseDTO.CompetitionProtocolsDTO{
		CompetitionResponseDTO: *s.competitionSerializer.Serialize(ctx, protocols.Competition),
		Races:                  make([]*racesResponstDTO.RaceOrdersResponseDTO, 0, len(protocols.Races)),
		Relays:                 make([]*relaysResponseDTO.RelayOrdersResponseDTO, 0, len(protocols.Relays)),
		Cryathlons:             make([]*cryathlonsResponseDTO.CryathlonOrdersResponseDTO, 0, len(protocols.Cryathlons)),
		Aquathlons:             make([]*aquathlonsResponseDTO.AquathlonOrdersResponseDTO, 0, len(protocols.Aquathlons)),
	}

	for _, raceOrder := range protocols.Races {
		response.Races = append(response.Races, s.raceOrdersSerializer.Serialize(ctx, raceOrder))
	}

	for _, relayOrder := range protocols.Relays {
		response.Relays = append(response.Relays, s.relaysOrdersSerializer.Serialize(ctx, relayOrder))
	}

	for _, cryathlonOrder := range protocols.Cryathlons {
		response.Cryathlons = append(response.Cryathlons, s.cryathlonOrdersSerializer.Serialize(ctx, cryathlonOrder))
	}

	for _, aquathlonOrder := range protocols.Aquathlons {
		response.Aquathlons = append(response.Aquathlons, s.aquathlonOrdersSerializer.Serialize(ctx, aquathlonOrder))
	}

	return response
}

func NewCompetitionProtocolsSerializer(
	competitionSerializer *CompetitionSerializer,
	raceOrdersSerializer *racesSerializers.RaceOrdersSerializer,
	relaysOrdersSerializer *relaysSerializers.RelayOrdersSerializer,
	cryathlonOrdersSerializer *cryathlonsSerializers.CryathlonOrdersSerializer,
	aquathlonOrdersSerializer *aquathlonsSerializers.AquathlonOrdersSerializer,
) *CompetitionProtocolsSerializer {
	return &CompetitionProtocolsSerializer{
		competitionSerializer:     competitionSerializer,
		raceOrdersSerializer:      raceOrdersSerializer,
		relaysOrdersSerializer:    relaysOrdersSerializer,
		cryathlonOrdersSerializer: cryathlonOrdersSerializer,
		aquathlonOrdersSerializer: aquathlonOrdersSerializer,
	}
}
