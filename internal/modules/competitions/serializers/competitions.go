package serializers

import (
	"context"
	"time"

	"gitlab.com/walrus-project/walrus-competition-api/internal/models"
	aquathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/dto/response"
	aquathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/serializers"
	responseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/dto/response"
	cryathlonsResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/dto/response"
	cryathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/serializers"
	racesResponstDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/dto/response"
	racesSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/serializers"
	relaysResponseDTO "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/dto/response"
	relaysSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/serializers"
)

type CompetitionSerializer struct {
	raceSerializer       *racesSerializers.RaceSerializer
	relaySerializer      *relaysSerializers.RelaySerializer
	aquathlonSerializers *aquathlonsSerializers.AquathlonSerializer
	cryathlonSerializers *cryathlonsSerializers.CryathlonSerializer
}

func (s *CompetitionSerializer) Serialize(ctx context.Context, competition *models.Competition) *responseDTO.CompetitionResponseDTO {
	response := &responseDTO.CompetitionResponseDTO{
		Id:          competition.ID,
		Name:        competition.Name,
		Description: competition.Description.String,
		StartDate:   competition.StartDate.Format(time.RFC3339),
		EndDate:     competition.EndDate.Format(time.RFC3339),
		Races:       make([]*racesResponstDTO.RaceResponseDTO, 0, len(competition.R.Races)),
		Relays:      make([]*relaysResponseDTO.RelayResponseDTO, 0, len(competition.R.Relays)),
		Aquathlons:  make([]*aquathlonsResponseDTO.AquathlonResponseDTO, 0, len(competition.R.Aquathlons)),
		Cryathlons:  make([]*cryathlonsResponseDTO.CryathlonResponseDTO, 0, len(competition.R.Cryathlons)),
	}

	for _, race := range competition.R.Races {
		response.Races = append(response.Races, s.raceSerializer.Serialize(ctx, race))
	}

	for _, relay := range competition.R.Relays {
		response.Relays = append(response.Relays, s.relaySerializer.Serialize(ctx, relay))
	}

	for _, aquathlon := range competition.R.Aquathlons {
		response.Aquathlons = append(response.Aquathlons, s.aquathlonSerializers.Serialize(ctx, aquathlon))
	}

	for _, cryathlon := range competition.R.Cryathlons {
		response.Cryathlons = append(response.Cryathlons, s.cryathlonSerializers.Serialize(ctx, cryathlon))
	}

	return response
}

func NewCompetitionSerializer(raceSerializer *racesSerializers.RaceSerializer, relaySerializer *relaysSerializers.RelaySerializer, aquathlonSerializers *aquathlonsSerializers.AquathlonSerializer, cryathlonSerializers *cryathlonsSerializers.CryathlonSerializer) *CompetitionSerializer {
	return &CompetitionSerializer{
		raceSerializer:       raceSerializer,
		relaySerializer:      relaySerializer,
		aquathlonSerializers: aquathlonSerializers,
		cryathlonSerializers: cryathlonSerializers,
	}
}
