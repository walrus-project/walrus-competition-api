package providers

import (
	"context"
	"database/sql"
	"fmt"

	_ "github.com/volatiletech/sqlboiler/v4/drivers/sqlboiler-psql/driver"

	dbConfig "gitlab.com/walrus-project/walrus-competition-api/configs/db"
)

func NewPostgreSQLProvider(ctx context.Context, config *dbConfig.Config) (*sql.DB, error) {
	db, err := sql.Open("postgres", config.DSN())
	if err != nil {
		return nil, fmt.Errorf("failed to open database connection: %w", err)
	}

	if err = db.PingContext(ctx); err != nil {
		return nil, fmt.Errorf("failed to ping database connection: %w", err)
	}

	return db, nil
}
