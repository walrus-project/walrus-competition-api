ARG IMG_GO=golang:1.18-alpine
ARG IMG_ALPINE=alpine:3.15

FROM $IMG_GO AS builder

RUN apk add --no-cache --update git \
    build-base

WORKDIR /app

COPY ./ .

RUN make vendor && make build-walrus-competition-api

FROM $IMG_ALPINE as image-walrus-competition-api

WORKDIR /app

COPY --from=builder /app/out/bin/walrus-competition-api ./
COPY --from=builder /app/templates ./

VOLUME ["/app/configs"]

RUN chmod +x /app/walrus-competition-api

EXPOSE 3000

ENTRYPOINT ["./walrus-competition-api"]