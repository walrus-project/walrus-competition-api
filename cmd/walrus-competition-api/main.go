package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"

	appConfig "gitlab.com/walrus-project/walrus-competition-api/configs/app"
	dbConfig "gitlab.com/walrus-project/walrus-competition-api/configs/db"
	mailConfig "gitlab.com/walrus-project/walrus-competition-api/configs/mail"
	ordersConfig "gitlab.com/walrus-project/walrus-competition-api/configs/orders"
	"gitlab.com/walrus-project/walrus-competition-api/internal/common/logger"
	migrateUtils "gitlab.com/walrus-project/walrus-competition-api/internal/common/utils/migrate"
	"gitlab.com/walrus-project/walrus-competition-api/internal/common/validator"
	aquathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/serializers"
	aquathlonsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/aquathlons/services"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions"
	competitionsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/competitions/services"
	cryathlonsSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/serializers"
	cryathlonsServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/cryathlons/services"
	"gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders"
	ordersSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/orders/serializers"
	racesSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/serializers"
	racesServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/races/services"
	relaysSerializers "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/serializers"
	relaysServices "gitlab.com/walrus-project/walrus-competition-api/internal/modules/relays/services"
	"gitlab.com/walrus-project/walrus-competition-api/internal/providers"
)

func main() {
	//	Init context
	ctx, cancel := context.WithCancel(context.Background())

	// Init logger
	l := logger.CreateLogger("walrus-competition-api").WithContext(ctx)

	//	Init validator
	validate, err := validator.CreateValidator()
	if err != nil {
		l.WithError(err).Fatal("failed to create validator")
	}

	//	Init database
	dbConf, err := dbConfig.New(validate)
	if err != nil {
		l.WithError(err).Fatal("failed to load database config")
	}

	db, err := providers.NewPostgreSQLProvider(ctx, dbConf)
	if err != nil {
		l.WithError(err).Fatal("failed init postgresql database provider")
	}

	l.Info("successfully connected to postgresql database")

	if err = migrateUtils.Migrate(db); err != nil {
		l.WithError(err).Fatal("db migrate failed")
	}

	// Init mail
	mailConf, err := mailConfig.New(validate)
	if err != nil {
		l.WithError(err).Fatal("failed to load mail config")
	}

	//	Init app
	appConf, err := appConfig.New()
	if err != nil {
		l.WithError(err).Fatal("failed to load application config")
	}

	r := chi.NewRouter()
	r.Use(middleware.SetHeader("Content-Type", "application/json"))
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins: []string{"https://*", "http://*"},
		AllowedMethods: []string{"GET", "POST", "OPTIONS"},
	}))

	//	Init common services, serializers and modules
	ordersConf, err := ordersConfig.New(validate)
	if err != nil {
		l.WithError(err).Fatal("failed to load orders config")
	}

	competitionsService := competitionsServices.NewCompetitionsService(db)
	racesOrdersService := racesServices.NewRacesOrdersService(db)
	relaysOrderService := relaysServices.NewRelaysOrdersService(db)
	aquathlonsOrdersService := aquathlonsServices.NewAquathlonsOrdersService(db)
	cryathlonsOrdersService := cryathlonsServices.NewCryathlonsOrdersService(db)
	raceSerializer := &racesSerializers.RaceSerializer{}
	relaySerializer := &relaysSerializers.RelaySerializer{}
	aquathlonSerializer := &aquathlonsSerializers.AquathlonSerializer{}
	cryathlonSerializer := &cryathlonsSerializers.CryathlonSerializer{}
	orderSerialzier := &ordersSerializers.OrderSerializer{}
	raceOrdersSerializer := racesSerializers.NewRaceOrdersSerializer(raceSerializer, orderSerialzier)
	relaysOrdersSerializer := relaysSerializers.NewRelayOrdersSerializer(relaySerializer, orderSerialzier)
	cryathlonOrdersSerializer := cryathlonsSerializers.NewCyrathlonOrdersSerializer(cryathlonSerializer, orderSerialzier)
	aquathlonOrdersSerializer := aquathlonsSerializers.NewAquathlonOrdersSerializer(aquathlonSerializer, orderSerialzier)

	if err = competitions.Init(
		r,
		db,
		competitionsService,
		racesOrdersService,
		relaysOrderService,
		aquathlonsOrdersService,
		cryathlonsOrdersService,
		raceSerializer,
		relaySerializer,
		aquathlonSerializer,
		cryathlonSerializer,
		raceOrdersSerializer,
		relaysOrdersSerializer,
		cryathlonOrdersSerializer,
		aquathlonOrdersSerializer,
	); err != nil {
		l.WithError(err).Fatal("failed to init competitions module")
	}

	if err = orders.Init(r, l, db, validate, competitionsService, orderSerialzier, mailConf, ordersConf); err != nil {
		l.WithError(err).Fatal("failed to init orders module")
	}

	server := &http.Server{Addr: appConf.Address(), Handler: r}

	//	Subscribe to system signals
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan,
		os.Interrupt,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()

		stop := <-signalChan

		l.WithField("signal", stop).Info("waiting for all processes to stop")

		var stopErr error
		if stopErr = server.Shutdown(ctx); stopErr != nil {
			l.WithError(err).Fatal("failed to shutdown server")
		}

		cancel()

		if stopErr = db.Close(); stopErr != nil {
			l.WithError(err).Fatal("failed to close postgresql database connection")
		}
	}()

	// Run the server
	l.Info(fmt.Sprintf("starting walrus competition api http server on: %s", appConf.Address()))

	if err = server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		l.WithError(err).Fatal("failed to listen local network address")
	}

	wg.Wait()
	l.Info("walrus competition api stopped")
}
