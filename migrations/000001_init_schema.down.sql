--  types
DROP TYPE IF EXISTS gender;
DROP TYPE IF EXISTS swimming_style;
DROP TYPE IF EXISTS order_status;

--  competitions
DROP TABLE IF EXISTS competitions;
DROP SEQUENCE IF EXISTS competitions_id_seq;

--  cryathlons
DROP TABLE IF EXISTS cryathlons;
DROP SEQUENCE IF EXISTS cryathlons_id_seq;

--  aquathlons
DROP TABLE IF EXISTS aquathlons;
DROP SEQUENCE IF EXISTS aquathlons_id_seq;

--  races
DROP TABLE IF EXISTS races;
DROP SEQUENCE IF EXISTS races_id_seq;

--  relays
DROP TABLE IF EXISTS relays;
DROP SEQUENCE IF EXISTS relays_id_seq;

--  orders
DROP TABLE IF EXISTS orders;
DROP SEQUENCE IF EXISTS orders_id_seq;

--  orders relations
--  order cryathlons
DROP TABLE IF EXISTS order_cryathlons;
ALTER TABLE order_cryathlons DROP CONSTRAINT IF EXISTS order_cryathlons_pkey;
DROP INDEX IF EXISTS order_cryathlons_idx;

--  order aquathlons
DROP TABLE IF EXISTS order_aquathlons;
ALTER TABLE order_aquathlons DROP CONSTRAINT IF EXISTS order_aquathlons_pkey;
DROP INDEX IF EXISTS order_aquathlons_idx;

--  order races
DROP TABLE IF EXISTS order_races;
ALTER TABLE order_races DROP CONSTRAINT IF EXISTS order_races_pkey;
DROP INDEX IF EXISTS order_races_idx;

--  order relays
DROP TABLE IF EXISTS order_relays;
ALTER TABLE order_relays DROP CONSTRAINT IF EXISTS order_relays_pkey;
DROP INDEX IF EXISTS order_relays_idx;