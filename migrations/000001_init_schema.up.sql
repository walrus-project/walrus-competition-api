--  types
CREATE TYPE gender AS ENUM (
    'male',
    'female',
    'all'
);

CREATE TYPE swimming_style AS ENUM (
    'breaststroke',
    'crawl',
    'freestyle',
    'butterfly'
);

CREATE TYPE order_status AS ENUM (
    'new',
    'accepted',
    'payment',
    'rejected'
);

--  competitions
CREATE TABLE competitions (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying,
    start_date date NOT NULL,
    end_date date NOT NULL
);

CREATE SEQUENCE competitions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE competitions_id_seq OWNED BY competitions.id;

ALTER TABLE ONLY competitions ALTER COLUMN id SET DEFAULT nextval('competitions_id_seq');

SELECT setval('competitions_id_seq', 1);

ALTER TABLE competitions ADD CONSTRAINT competitions_pkey PRIMARY KEY (id);

-- cryathlons
CREATE TABLE cryathlons (
    id integer NOT NULL,
    name character varying NOT NULL,
    run_distance integer NOT NULL,
    ski_distance integer NOT NULL,
    water_distance integer NOT NULL,
    barefoot_distance integer NOT NULL,
    gender gender NOT NULL,
    date date NOT NULL,
    competition_id integer
);

CREATE SEQUENCE cryathlons_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE cryathlons_id_seq OWNED BY cryathlons.id;

ALTER TABLE ONLY cryathlons ALTER COLUMN id SET DEFAULT nextval('cryathlons_id_seq');

SELECT setval('cryathlons_id_seq', 1);

ALTER TABLE ONLY cryathlons ADD CONSTRAINT cryathlons_pkey PRIMARY KEY (id);
ALTER TABLE ONLY cryathlons ADD CONSTRAINT cryathlons_fkey FOREIGN KEY (competition_id) REFERENCES competitions(id);

--  aquathlons
CREATE TABLE aquathlons (
    id integer NOT NULL,
    name character varying NOT NULL,
    run_distance integer NOT NULL,
    water_distance integer NOT NULL,
    gender gender NOT NULL,
    date date NOT NULL,
    competition_id integer
);

CREATE SEQUENCE aquathlons_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE aquathlons_id_seq OWNED BY aquathlons.id;

ALTER TABLE ONLY aquathlons ALTER COLUMN id SET DEFAULT nextval('aquathlons_id_seq');

SELECT setval('aquathlons_id_seq', 1);

ALTER TABLE ONLY aquathlons ADD CONSTRAINT aquathlons_pkey PRIMARY KEY (id);
ALTER TABLE ONLY aquathlons ADD CONSTRAINT aquathlons_fkey FOREIGN KEY (competition_id) REFERENCES competitions(id);

-- races
CREATE TABLE races (
    id integer NOT NULL,
    name character varying NOT NULL,
    distance integer NOT NULL,
    swimming_style swimming_style NOT NULL,
    gender gender NOT NULL,
    min_age integer,
    max_age integer,
    para_swimmers boolean NOT NULL,
    date date NOT NULL,
    competition_id integer
);

CREATE SEQUENCE races_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE races_id_seq OWNED BY races.id;

ALTER TABLE ONLY races ALTER COLUMN id SET DEFAULT nextval('races_id_seq');

SELECT setval('races_id_seq', 1);

ALTER TABLE ONLY races ADD CONSTRAINT races_pkey PRIMARY KEY (id);
ALTER TABLE ONLY races ADD CONSTRAINT races_fkey FOREIGN KEY (competition_id) REFERENCES competitions(id);

--  relays
CREATE TABLE relays (
    id integer NOT NULL,
    name character varying NOT NULL,
    distance integer NOT NULL,
    count integer NOT NULL,
    date date NOT NULL,
    competition_id integer,
    swimming_style swimming_style NOT NULL
);

CREATE SEQUENCE relays_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE relays_id_seq OWNED BY relays.id;

ALTER TABLE ONLY relays ALTER COLUMN id SET DEFAULT nextval('relays_id_seq');

SELECT setval('relays_id_seq', 1);

ALTER TABLE ONLY relays ADD CONSTRAINT relays_pkey PRIMARY KEY (id);
ALTER TABLE ONLY relays ADD CONSTRAINT relays_fkey FOREIGN KEY (competition_id) REFERENCES competitions(id);

--  orders
CREATE TABLE orders (
    id integer NOT NULL,
    last_name character varying NOT NULL,
    first_name character varying NOT NULL,
    middle_name character varying,
    birthdate date NOT NULL,
    gender gender NOT NULL,
    para_swimmer boolean NOT NULL,
    club_name character varying NOT NULL,
    location character varying NOT NULL,
    email character varying NOT NULL,
    phone character varying,
    additional character varying,
    status order_status NOT NULL,
    created_at timestamp without time zone NOT NULL,
    competition_id integer NOT NULL,
    need_skis boolean NOT NULL DEFAULT false
);

CREATE SEQUENCE orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE orders_id_seq OWNED BY orders.id;

ALTER TABLE ONLY orders ALTER COLUMN id SET DEFAULT nextval('orders_id_seq');

SELECT setval('orders_id_seq', 1);

ALTER TABLE ONLY orders ADD CONSTRAINT orders_pkey PRIMARY KEY (id);
ALTER TABLE ONLY orders ADD CONSTRAINT orders_fkey FOREIGN KEY (competition_id) REFERENCES competitions(id);

--  orders relations
--  order cryathlons
CREATE TABLE order_cryathlons (
    cryathlon_id integer NOT NULL,
    order_id integer NOT NULL
);

ALTER TABLE ONLY order_cryathlons ADD CONSTRAINT order_cryathlons_pkey PRIMARY KEY (cryathlon_id, order_id);

CREATE INDEX order_cryathlons_idx ON order_cryathlons USING btree(order_id);

ALTER TABLE ONLY order_cryathlons ADD CONSTRAINT order_cryathlons_cryathlon_fkey FOREIGN KEY (cryathlon_id) REFERENCES cryathlons(id) ON UPDATE CASCADE;
ALTER TABLE ONLY order_cryathlons ADD CONSTRAINT order_cryathlons_order_fkey FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE;

--  order aquathlons
CREATE TABLE order_aquathlons (
    aquathlon_id integer NOT NULL,
    order_id integer NOT NULL
);

ALTER TABLE ONLY order_aquathlons ADD CONSTRAINT order_aquathlons_pkey PRIMARY KEY (aquathlon_id, order_id);

CREATE INDEX order_aquathlons_idx ON order_aquathlons USING btree(aquathlon_id);

ALTER TABLE ONLY order_aquathlons ADD CONSTRAINT order_aquathlons_aquathlon_fkey FOREIGN KEY (aquathlon_id) REFERENCES aquathlons(id) ON UPDATE CASCADE;
ALTER TABLE ONLY order_aquathlons ADD CONSTRAINT order_aquathlons_order_fkey FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE;

--  order races
CREATE TABLE order_races (
    race_id integer NOT NULL,
    order_id integer NOT NULL
);

ALTER TABLE ONLY order_races ADD CONSTRAINT order_races_pkey PRIMARY KEY (race_id, order_id);

CREATE INDEX order_races_idx ON order_races USING btree(race_id);

ALTER TABLE ONLY order_races ADD CONSTRAINT order_races_race_fkey FOREIGN KEY (race_id) REFERENCES races(id) ON UPDATE CASCADE;
ALTER TABLE ONLY order_races ADD CONSTRAINT order_races_order_fkey FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE;

--  order relays
CREATE TABLE order_relays (
    relay_id integer NOT NULL,
    order_id integer NOT NULL
);

ALTER TABLE ONLY order_relays ADD CONSTRAINT order_relays_pkey PRIMARY KEY (relay_id, order_id);

CREATE INDEX order_relays_idx ON order_relays USING btree(relay_id);

ALTER TABLE ONLY order_relays ADD CONSTRAINT order_relays_relay_fkey FOREIGN KEY (relay_id) REFERENCES relays(id) ON UPDATE CASCADE;
ALTER TABLE ONLY order_relays ADD CONSTRAINT order_relays_order_fkey FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE;