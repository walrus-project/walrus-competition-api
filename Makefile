BUILD_DIR=./out/bin
BINARY_NAME=walrus-competition-api
BUILD_GOOS=linux
BUILD_GOARCH=amd64

#	Go section
.PHONY: build-app
build-app:
	mkdir -p ${BUILD_DIR}
	cd ${BUILD_DIR}
	GOOS=${BUILD_GOOS} GOARCH=${BUILD_GOARCH} CGO_ENABLED=1 GO111MODULE=on go build -v -race -mod vendor -o ${BUILD_DIR}/${OUT_NAME} ./cmd/${APP_NAME}/main.go

.PHONY: build-walrus-competition-api
build-walrus-competition-api:
	OUT_NAME=${BINARY_NAME} APP_NAME=walrus-competition-api $(MAKE) build-app

.PHONY: clean
clean:
	go clean
	rm -rf ./out

.PHONY: vendor
vendor:
	go mod vendor

.PHONY: install-deps
install-deps:
	go get ./...

.PHONY: vet
vet:
	go vet

.PHONY: lint
lint:
	golangci-lint run

.PHONY: lint-fix
lint-fix:
	golangci-lint run --fix

#	Database section
.PHONY: migrate-up
migrate-up:
	migrate -path ./migrations -database "$(DATABASE_URL)" -verbose up

.PHONY: migrate-down
migrate-down:
	migrate -path ./migrations -database "$(DATABASE_URL)" -verbose down

.PHONE: boil-generate
boil-generate:
	sqlboiler psql